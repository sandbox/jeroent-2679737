<?php

/**
 * @file
 * Contains \Drupal\epsacrop\EpsacropManager.
 */

namespace Drupal\epsacrop;

use Drupal\Core\Database\Connection;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\field\Entity\FieldConfig;
use Symfony\Component\HttpFoundation\RequestStack;

/**
 * The Epsacrop manager.
 */
class EpsacropManager implements EpsacropManagerInterface {

  /**
   * The database connection.
   *
   * @var \Drupal\Core\Database\Connection
   */
  protected $connection;

  /**
   * The image style storage.
   *
   * @var \Drupal\Core\Entity\EntityStorageInterface
   */
  protected $imageStyleStorage;

  /**
   * The module handler.
   *
   * @var \Drupal\Core\Extension\ModuleHandlerInterface
   */
  protected $moduleHandler;

  /**
   * The request stack.
   *
   * @var \Symfony\Component\HttpFoundation\RequestStack
   */
  protected $requestStack;

  /**
   * EpsacropManager constructor.
   *
   * @param \Drupal\Core\Database\Connection
   *   The database connection.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   * @param \Drupal\Core\Extension\ModuleHandlerInterface $module_handler
   *   The module handler.
   * @param \Symfony\Component\HttpFoundation\RequestStack $request_stack
   *   The request stack.
   */
  public function __construct(Connection $connection, EntityTypeManagerInterface $entity_type_manager, ModuleHandlerInterface $module_handler, RequestStack $request_stack) {
    $this->connection = $connection;
    $this->imageStyleStorage = $entity_type_manager->getStorage('image_style');
    $this->moduleHandler = $module_handler;
    $this->requestStack = $request_stack;
  }

  /**
   * {@inheritdoc}
   */
  public function loadStyle($style_name) {
    if (empty($style_name)) {
      return FALSE;
    }

    $styles = $this->loadStyles();
    if (isset($styles[$style_name]) && !empty($styles[$style_name])) {
      return $styles[$style_name];
    }

    return FALSE;
  }

  /**
   * {@inheritdoc}
   */
  public function loadStyles() {
    $return = [];
    $styles = $this->imageStyleStorage->loadMultiple();

    foreach ($styles as $style_name => $style) {
      foreach ($style->getEffects() as $sid => $effect) {
        if ($effect->getPluginId() == 'epsacrop_crop') {
          $return[$style_name] = $style;
          break;
        }
      }
    }

    return $return;
  }

  /**
   * {@inheritdoc}
   */
  public function loadStylesByInstance($entity_type, $field_name, $bundle) {
    $instance = FieldConfig::loadByName($entity_type, $bundle, $field_name);
    $styles = $instance->getThirdPartySetting('epsacrop', 'styles', []);
    $styles = array_filter($styles);

    // Allow other modules to alter list of styles
    $this->moduleHandler->alter('epsacrop_load_styles', $styles, $entity_type, $field_name, $bundle);

    return $styles;
  }

  /**
   * {@inheritdoc}
   */
  public function getEffect($style) {
    foreach ($style->getEffects() as $eid => $effect) {
      if ($effect->getPluginId() == 'epsacrop_crop') {
        return [$style->id() => $effect];
        break;
      }
    }
    return FALSE;
  }

  /**
   * {@inheritdoc}
   */
  public function getStyleNameFromUrl() {
    $split = explode('/', $this->requestStack->getCurrentRequest()->getRequestUri());
    $pointer = array_search('styles', $split);
    if ($pointer !== FALSE) {
      return $split[++$pointer];
    }
    return FALSE;
  }

  /**
   * {@inheritdoc}
   */
  public function deleteFile($fid) {
   return $this->connection->delete('epsacrop_files')
      ->condition('fid', $fid)
      ->execute();
  }

  /**
   * {@inheritdoc}
   */
  public function fidExists($fid) {
    if (!empty($fid)) {
      return count($this->connection->select('epsacrop_files', 'e')
        ->fields('e')
        ->condition('fid', $fid)
        ->execute()
        ->fetchAll());
    }

    return 0;
  }

  /**
   * {@inheritdoc}
   */
  public function addFile($fid, $coords) {
    $this->connection->insert('epsacrop_files')
      ->fields(array(
        'fid' => $fid,
        'coords' => serialize($coords)
      ))
      ->execute();
  }

  /**
   * {@inheritdoc}
   */
  public function getCoordsfromFid($fid) {
    $files = &drupal_static(__FUNCTION__);
    if (empty($files[$fid])) {
      $files[$fid] = new \stdClass;
      $has_coords = $this->fidExists($fid);
      if ($has_coords != 0) {
        $result = $this->connection->select('epsacrop_files', 'e')
          ->fields('e', array('coords'))
          ->condition('e.fid', $fid)
          ->execute()
          ->fetchField();
        $files[$fid] = unserialize($result);
      }
      return $files[$fid];
    }

    return $files[$fid];
  }

  /**
   * {@inheritdoc}
   */
  public function getUriFromFid($fid) {
    if (!empty($fid) && is_numeric($fid)) {
      return (string) $this->connection->select('file_managed', 'f')
        ->fields('f', array('uri'))
        ->condition('f.fid', $fid)
        ->range(0, 1)
        ->execute()
        ->fetchField();
    }

    return FALSE;
  }

  /**
   * {@inheritdoc}
   */
  public function saveCoords($fid, $coords) {
    $affected = $this->connection->update('epsacrop_files')
      ->fields(array(
        'coords' => serialize($coords)
      ))
      ->condition('fid', $fid)
      ->execute();

    if (($affected == 0) && ($this->fidExists($fid) == 0)) {
     $this->addFile($fid, $coords);
    }

    image_path_flush($this->getUriFromFid($fid));
  }

}
