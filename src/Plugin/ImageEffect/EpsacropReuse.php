<?php

/**
 * @file
 * Contains \Drupal\epsacrop\Plugin\ImageEffect\EpsacropReuse.
 */

namespace Drupal\epsacrop\Plugin\ImageEffect;

use Drupal\Component\Serialization\Json;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Image\ImageInterface;
use Drupal\Core\Routing\CurrentRouteMatch;
use Drupal\epsacrop\EpsacropManagerInterface;
use Drupal\image\ConfigurableImageEffectBase;
use Drupal\image\Entity\ImageStyle;
use Drupal\image\Plugin\ImageEffect\CropImageEffect;
use Psr\Log\LoggerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\RequestStack;

/**
 * crops an image resource by used chosen dimensions.
 *
 * @ImageEffect(
 *   id = "epsacrop_reuse",
 *   label = @Translation("Reuse EPSA Image Crop"),
 *   description = @Translation("Use the image crop selection from another image style")
 * )
 */
class EpsacropReuse extends ConfigurableImageEffectBase {

  /**
   * The epsacrop manager.
   *
   * @var \Drupal\epsacrop\EpsacropManagerInterface
   */
  protected $epsacropManager;

  /**
   * The current route match.
   *
   * @var \Drupal\Core\Routing\CurrentRouteMatch
   */
  protected $currentRouteMatch;

  /**
   * Constructs a Drupal\epsacrop\Plugin\ImageEffect\EpsacropReuse object.
   *
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin_id for the plugin instance.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param \Psr\Log\LoggerInterface $logger
   *   A logger instance.
   * @param \Drupal\epsacrop\EpsacropManagerInterface $epsacrop_manager
   *   The epsacrop manager.
   * @param \Drupal\Core\Routing\CurrentRouteMatch $current_route_match
   *   The current route match.
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, LoggerInterface $logger, EpsacropManagerInterface $epsacrop_manager, CurrentRouteMatch $current_route_match) {
    parent::__construct($configuration, $plugin_id, $plugin_definition, $logger);

    $this->epsacropManager = $epsacrop_manager;
    $this->currentRouteMatch = $current_route_match;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('logger.factory')->get('image'),
      $container->get('epsacrop.manager'),
      $container->get('current_route_match')
    );
  }


  /**
   * {@inheritdoc}
   */
  public function applyEffect(ImageInterface $image) {
    if (empty($this->configuration['jcrop_reuse'])) {
      return;
    }
    $style = $this->epsacropManager->loadStyle($this->configuration['jcrop_reuse']);
    foreach($style->getEffects() as $effect) {
      if ($effect->getPluginId() == 'epsacrop_crop') {
        $effect->applyEffect($image, $style->getName());
        return TRUE;
      }
    }
    return FALSE;
  }

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration() {
    return parent::defaultConfiguration() + [
      'jcrop_reuse' => '',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state) {
    $styles = array_keys($this->epsacropManager->loadStyles());
    $styles = array_combine($styles, $styles);
    //exclude the current style
    $current_image_style = $this->currentRouteMatch->getParameter('image_style');
    if ($key = array_search($current_image_style->getName(), $styles)) {
      unset($styles[$key]);
    }
    if (count($styles) > 0) {
      $form['jcrop_reuse'] = array(
        '#title' => t('Reuse the crop selection from'),
        '#type' => 'select',
        '#options' => $styles,
        '#default_value' => $this->configuration['jcrop_reuse'],
      );
    }
    else {
      $form['jcrop_reuse_message'] = array(
        '#value' => t('No styles were found currently using EPSA crop. Please create a style with EPSA crop first.'),
      );
    }

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function validateConfigurationForm(array &$form, FormStateInterface $form_state) {
  }

  /**
   * {@inheritdoc}
   */
  public function submitConfigurationForm(array &$form, FormStateInterface $form_state) {
    $this->configuration['jcrop_reuse'] = $form_state->getValue('jcrop_reuse');
  }

}
