<?php

/**
 * @file
 * Contains \Drupal\epsacrop\Plugin\ImageEffect\EpsaCropCrop.
 */

namespace Drupal\epsacrop\Plugin\ImageEffect;

use Drupal\Component\Serialization\Json;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Image\ImageInterface;
use Drupal\epsacrop\EpsacropManagerInterface;
use Drupal\image\Plugin\ImageEffect\CropImageEffect;
use Psr\Log\LoggerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * crops an image resource by used chosen dimensions.
 *
 * @ImageEffect(
 *   id = "epsacrop_crop",
 *   label = @Translation("EPSA Image Crop"),
 * )
 */
class EpsaCropCrop extends CropImageEffect {

  /**
   * The epsacrop manager.
   *
   * @var \Drupal\epsacrop\EpsacropManagerInterface
   */
  protected $epsacropManager;

  /**
   * The file storage.
   *
   * @var \Drupal\Core\Entity\EntityStorageInterface
   */
  protected $fileStorage;

  /**
   * The module handler.
   *
   * @var \Drupal\Core\Extension\ModuleHandlerInterface
   */
  protected $moduleHandler;

  /**
   * Constructs a Drupal\epsacrop\Plugin\ImageEffect\EpsaCropCrop object.
   *
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin_id for the plugin instance.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param \Psr\Log\LoggerInterface $logger
   *   A logger instance.
   * @param \Drupal\epsacrop\EpsacropManagerInterface $epsacrop_manager
   *   The epsacrop manager.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   * @param \Drupal\Core\Extension\ModuleHandlerInterface $module_handler
   *   The module handler.
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, LoggerInterface $logger, EpsacropManagerInterface $epsacrop_manager, EntityTypeManagerInterface $entity_type_manager, ModuleHandlerInterface $module_handler) {
    parent::__construct($configuration, $plugin_id, $plugin_definition, $logger);

    $this->epsacropManager = $epsacrop_manager;
    $this->fileStorage = $entity_type_manager->getStorage('file');
    $this->moduleHandler = $module_handler;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('logger.factory')->get('image'),
      $container->get('epsacrop.manager'),
      $container->get('entity_type.manager'),
      $container->get('module_handler')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function applyEffect(ImageInterface $image, $style_name = '') {
    $files = $this->fileStorage->loadByProperties(['uri' => $image->getSource()]);
    $file = reset($files);
    if (!empty($file) && $file->id() > 0) {
      $coords = $this->epsacropManager->getCoordsfromFid($file->id());

      if (is_string($coords)) {
        $coords = Json::decode($coords);
        if (is_object($coords)) {
          $coords = (array) $coords;
        }
      }
      if (!empty($coords) && is_array($coords)) {
        if(empty($style_name)) {
          $style_name = $this->epsacropManager->getStyleNameFromUrl();
        }
        if (!empty($style_name)) {
          // Trigger presave hook.
          $this->moduleHandler->invokeAll('epsacrop_crop_image_presave', [$image, $this->configuration, $coords, $style_name]);

          $style = $this->epsacropManager->loadStyle($style_name);
          if (!empty($style)) {
            $effect = $this->epsacropManager->getEffect($style);
            if (!empty($effect)) {
              $preset = 'epsacrop-' . $style_name;
              $coord = isset($coords[$file->id()][$preset]) ? $coords[$file->id()][$preset] : '';
              if (!empty($coord)) {
                if ($image->crop($coord['x'], $coord['y'], $coord['w'], $coord['h'])) {
                  // Trigger postsave hook.
                  $this->moduleHandler->invokeAll('epsacrop_crop_image_postsave', [$image, $this->configuration, $coords, $style_name]);
                  if (isset($this->configuration['disable_scale']) && $this->configuration['disable_scale']) {
                    return TRUE;
                  }

                  return $image->scale($this->configuration['width'], $this->configuration['height']);
                }
              }
            }
          }
        }
      }
    }

    list($x, $y) = explode('-', $this->configuration['anchor']);
    return (empty($this->configuration['jcrop_settings']['fallback'])) ? $image->crop($x, $y, $this->configuration['width'], $this->configuration['height']) : TRUE;
  }

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration() {
    return parent::defaultConfiguration() + [
      'disable_scale' => 0,
      'jcrop_settings' => [
        'aspect_ratio' => '',
        'bgcolor' => 'black',
        'bgopacity' => '0.6',
        'fallback' => FALSE,
      ],
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state) {
    $form = parent::buildConfigurationForm($form, $form_state);

    // Override default
    $form['width']['#required'] = FALSE;
    $form['height']['#required'] = FALSE;

    // Change description for the anchor
    $form['anchor']['#description'] = t('The part of the image that will be retained during the crop if no coordinates are set.');

    $form['disable_scale'] = [
      '#type' => 'checkbox',
      '#title' => t('Disable image scaling'),
      '#description' => t('Only crop the image to the selected area and don\'t scale it. When enabled the above dimensions are used only when the cropping area is not selected.'),
      '#default_value' => $this->configuration['disable_scale'],
    ];

    $form['jcrop_settings'] = [
      '#type' => 'fieldset',
      '#title' => t('Advanced settings'),
      '#collapsed' => TRUE,
      '#collapsible' => TRUE,
    ];

    $form['jcrop_settings']['aspect_ratio'] = [
      '#type' => 'textfield',
      '#title' => t('Aspect Ratio'),
      '#description' => t('Aspect ratio of w/h (e.g. 1 for square)'),
      '#default_value' => $this->configuration['jcrop_settings']['aspect_ratio'],
      '#size' => 10,
    ];

    $form['jcrop_settings']['bgcolor'] = [
      '#type' => 'textfield',
      '#title' => t('Background color'),
      '#description' => t('Set color of background container'),
      '#default_value' => $this->configuration['jcrop_settings']['bgcolor'],
      '#required' => TRUE,
      '#size' => 10,
    ];

    $form['jcrop_settings']['bgopacity'] = [
      '#type' => 'textfield',
      '#title' => t('Background opacity'),
      '#description' => t('Opacity of outer image when cropping'),
      '#default_value' => $this->configuration['jcrop_settings']['bgopacity'],
      '#required' => TRUE,
      '#size' => 10,
    ];

    $form['jcrop_settings']['fallback'] = [
      '#type' => 'checkbox',
      '#title' => t('Ignore if not manually set'),
      '#description' => t('If a user does not manually set the crop, ignore these settings.  This is useful if you want to use other effects to set a default crop, which may then be overridden manually using EPSA Crop.  When this is checked, make sure the EPSA Crop effect is above any default cropping effects.'),
      '#default_value' => $this->configuration['jcrop_settings']['fallback'],
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function validateConfigurationForm(array &$form, FormStateInterface $form_state) {
    parent::validateConfigurationForm($form, $form_state);

    if (!empty($form_state->getValue('height')) && empty($form_state->getValue('width'))) {
      $form_state->setErrorByName('height', t('Height cannot be empty without a width value'));
    }

    if (!empty($form_state->getValue('width')) && empty($form_state->getValue('height'))) {
      $form_state->setErrorByName('width', t('Width cannot be empty without a height value'));
    }

    $aspect_ratio = $form_state->getValue('aspect_ratio');
    if (!empty($aspect_ratio)) {
      $parts = explode('/', $aspect_ratio);
      if (count($parts) == 2) {
        if (!is_numeric($parts[0]) || !is_numeric($parts[1])) {
          $form_state->setErrorByName('aspect_ratio', t('Both parts of @name must be an integer.', ['@name' => $form['aspect_ratio']['#title']]));
        }
      }
      elseif (!is_numeric($aspect_ratio)) {
        $form_state->setErrorByName('aspect_ratio', t('@name must be an integer.', ['@name' => $form['aspect_ratio']['#title']]));
      }
    }
  }

  /**
   * {@inheritdoc}
   */
  public function submitConfigurationForm(array &$form, FormStateInterface $form_state) {
    parent::submitConfigurationForm($form, $form_state);

    $this->configuration['disable_scale'] = $form_state->getValue('disable_scale');
    $this->configuration['jcrop_settings']['aspect_ratio'] = $form_state->getValue([
      'jcrop_settings',
      'aspect_ratio'
    ]);
    $this->configuration['jcrop_settings']['bgcolor'] = $form_state->getValue([
      'jcrop_settings',
      'bgcolor'
    ]);
    $this->configuration['jcrop_settings']['bgopacity'] = $form_state->getValue([
      'jcrop_settings',
      'bgopacity'
    ]);
    $this->configuration['jcrop_settings']['fallback'] = $form_state->getValue([
      'jcrop_settings',
      'fallback'
    ]);
  }

}
