<?php

/**
 * @file
 * Contains Drupal\epsacrop\EpsacropManagerInterface.
 */

namespace Drupal\epsacrop;

/**
 * Interface implemented by the Epsacrop manager.
 *
 * @see \Drupal\epsacrop\EpsacropManager.
 */
interface EpsacropManagerInterface {

  /**
   * This function load only on style that implement epsacrop effect.
   *
   * @param string $style_name
   *
   * @return bool
   */
  public function loadStyle($style_name);

  /**
   * Helper function that get all styles with epsacrop effect.
   *
   * @return array
   */
  public function loadStyles();

  /**
   * Get all style that are attach to one instance of field.
   *
   * @param string $entity_type
   * @param string $field_name
   * @param string $bundle
   *
   * @return array
   */
  public function loadStylesByInstance($entity_type, $field_name, $bundle);

  /**
   * Return the effect issue from the module.
   *
   * @param $style
   *
   * @return array|bool
   */
  public function getEffect($style);

  /**
   * Helper function that get the style name from image url.
   *
   * @return string|bool
   */
  public function getStyleNameFromUrl();

  /**
   * Delete the coordinates in the database.
   *
   * @param $fid
   *
   * @return int
   */
  public function deleteFile($fid);

  /**
   * Check if we've coordinates for one file.
   *
   * @param int $fid
   *
   * @return bool
   */
  public function fidExists($fid);

  /**
   * Insert coordinates for one file in the database.
   *
   * @param $fid
   * @param $coords
   *
   * @return mixed
   */
  public function addFile($fid, $coords);

  /**
   * Get all coordinates for one file.
   *
   * @param int $fid
   *
   * @return array
   */
  public function getCoordsfromFid($fid);

  /**
   * Try to get the uri from a fid.
   *
   * @param mixed $fid
   *
   * @return void
   */
  public function getUriFromFid($fid);

  /**
   * Save the coordinates in the database.
   *
   * @param int $fid
   * @param array $coords
   *
   * @return void
   */
  public function saveCoords($fid, $coords);
}
