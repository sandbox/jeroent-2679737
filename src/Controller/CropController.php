<?php

/**
 * @file
 * Contains \Drupal\epsacrop\Controller\CropController.
 */

namespace Drupal\epsacrop\Controller;

use Drupal\Component\Serialization\Json;
use Drupal\Core\Access\AccessResult;
use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Field;
use Drupal\Core\Render\RendererInterface;
use Drupal\Core\Url;
use Drupal\epsacrop\EpsacropManagerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

/**
 * Returns responses for epsacrop module routes.
 */
class CropController extends ControllerBase {

  /**
   * The epsacrop manager.
   *
   * @var \Drupal\epsacrop\EpsacropManagerInterface
   */
  protected $epsacropManager;

  /**
   * The renderer.
   *
   * @var \Drupal\Core\Render\RendererInterface
   */
  protected $renderer;

  /**
   * CropController constructor.
   *
   * @param \Drupal\epsacrop\EpsacropManagerInterface $epsacrop_manager
   *   The epsacrop manager.
   * @param \Drupal\Core\Render\RendererInterface $renderer
   *   The renderer.
   */
  public function __construct(EpsacropManagerInterface $epsacrop_manager, RendererInterface $renderer) {
    $this->epsacropManager = $epsacrop_manager;
    $this->renderer = $renderer;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('epsacrop.manager'),
      $container->get('renderer')
    );
  }

  /**
   * Crop dialog.
   *
   * @param $entity_name
   * @param $field_name
   * @param $bundle
   * @param $fid
   *
   * @return \Symfony\Component\HttpFoundation\Response
   */
  public function dialog($entity_name, $field_name, $bundle, $fid) {
    $i = 0;

    if ($entity_name == 'all' && $field_name == 'all' && $bundle == 'all') {
      //When on the file edit page, there is no entity_name, field_name or bundle. So make all styles available for editing
      $all_styles = array_keys($this->epsacropManager->loadStyles());
      $styles = ['styles' => array_combine($all_styles, $all_styles)];
    }
    else {
      $styles = $this->epsacropManager->loadStylesByInstance($entity_name, $field_name, $bundle);
    }

    $presets = [];
    foreach ($styles as $style_name) {
      $style = $this->epsacropManager->loadStyle($style_name);
      $effect = $this->epsacropManager->getEffect($style);
      $effect = current($effect);
      $effect_configuration = $effect->getConfiguration();
      $id = 'epsacrop-' . $style_name;
      $name = is_array($style) && !empty($style['label']) ? $style['label'] : $style_name;
      $presets[] = [
        '#type' => 'link',
        '#attributes' => [
          'data-width' => $effect_configuration['data']['width'],
          'data-height' => $effect_configuration['data']['height'],
          'data-bgcolor' => $effect_configuration['data']['jcrop_settings']['bgcolor'],
          'data-bgopacity' => $effect_configuration['data']['jcrop_settings']['bgopacity'],
          'data-aspect-ratio' => $effect_configuration['data']['jcrop_settings']['aspect_ratio'],
          'id' => $id,
          'class' => ($i++ == 0) ? [
            'js-epsacrop-preset-link',
            'selected',
          ] : ['js-epsacrop-preset-link'],
        ],
        '#title' => $name,
        '#url' => Url::fromRoute('<current>'),
      ];
    }
    $data = $this->epsacropManager->getCoordsfromFid($fid);
    if (!is_string($data)) {
      $data = Json::decode((object) $data);
    }

    $epsacrop_dialog = [
      '#theme' => 'epsacrop_dialog',
      '#data' => $data,
      '#presets' => $presets,
    ];

    return new Response($this->renderer->render($epsacrop_dialog));
  }

  /**
   * Epsacrop ajax callback.
   *
   * Retrieves or saves coordinates.
   *
   * @param $operation
   * @param $fid
   *
   * @return \Symfony\Component\HttpFoundation\JsonResponse
   */
  public function ajax(Request $request, $operation, $fid) {
    $return = NULL;
    switch ($operation) {
      case 'get':
        $return = $this->epsacropManager->getCoordsfromFid($fid);
        if ($return == FALSE) {
          $return = [];
        }
        break;
      case 'put':
        if (!empty($request->request->get('coords'))) {
          $coords = $request->request->get('coords');
          $this->epsacropManager->saveCoords($fid, $coords);
        }
        break;
      case 'del':

        break;
    }

    return new JsonResponse($return);
  }

  /**
   * Access check for the epsacrop pages.
   *
   * @return \Drupal\Core\Access\AccessResultInterface.
   */
  public function access() {
    $account = $this->currentUser();
    $permissions = ['epsacrop create crops', 'epsacrop edit all crops'];

    return AccessResult::allowedIfHasPermissions($account, $permissions, 'OR');
  }

}
